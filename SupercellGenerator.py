import os           # Allows ability to delete files so a temperary file can be used (Also makes post processing of the file a lot easier)
lat_len = 5.43071   #Units : Angstroms
Sicell = [[0,0,0],[0,0.5,0.5],[0.25,0.25,0.25],[0.5,0,0.5],[0.25,0.75,0.75],[0.75,0.25,0.75],[0.75,0.75,0.25],[0.5,0.5,0]]  # x,y,z fractional positions for the 8 Si atoms in the cell
Hcell = [[0.875,0.125,0.875],[0.625,0.375,0.875],[0.375,0.625,0.875],[0.125,0.875,0.875],[0.375,0.875,0.625],[0.625,0.875,0.375],[0.875,0.875,0.125]
         ,[0.875,0.625,0.375],[0.875,0.375,0.625],[0.125,0.125,0.125],[0.375,0.125,0.375],[0.125,0.375,0.375],[0.125,0.625,0.625],[0.375,0.375,0.125]
         ,[0.625,0.625,0.125],[0.125,0.625,0.625],[0.625,0.125,0.625],[-0.125,0.125,-0.125],[-0.125,-0.125,0.125],[0.125,-0.125,-0.125],[-0.125,0.375,0.625]
         ,[0.375,0.625,-0.125],[0.625,-0.125,0.375],[-0.125,0.625,0.375],[0.625,0.375,-0.125],[0.375,-0.125,0.625]]         # x,y,z fractional positions for the 26 possible H atoms in the cell

Hoff = 0.2          #this is in conventional units and represents the offset for the viable area for hydrogens to populate
Sioff = 0.5         #this is in conventional units and represents the offset for theviable area for Silicons to populate

def write_coords(atom,species): 
    f.write(str(species)+"   ")                     #Defines Atom type
    for i in range(0,3):
        if i%3 == 0:
            f.write(str((1/a)*(atom[i]+x))+"   ")   #Writes final fractional x co-ord in terms of supercell
        elif i%3 == 1:
            f.write(str((1/b)*(atom[i]+y))+"   ")   #Writes y co-ord
        else:
            f.write(str((1/c)*(atom[i]+z))+"   ")   #Writes z co-ord
    f.write("\n")                                   #Line break

def get_string(allowed,question):       #Asks question (a string) and checks if the answer is allowed with a list of allowed strings
    ans = None
    while ans not in allowed:
        ans =input(question+"\n").lower()    # Forces input to be lower case so caps lock isn't an issue
    return ans

def get_num(question,form):             #Asks question (a string) and converts it to a desired form ("int" for integer or anything else for float)
    while True:
        try:                            #This checks if the following lines of code work, if an error occurs, it moves to except
            value = float(input(question+"\n"))
            break                       # This allows the code to break from the while loop if no error is found
        except:
            print("Please ensure input is a number")
    if form == "int":
        return int(round(value))        # If an integer is required, will round the float then convert it
    else:
        return value                    # Otherwise, the float is returned

def remove_duplicates():
    lines = set()                       # Creates a set of already seen lines
    f = open(structure+"_"+str(width)+"_"+str(depth)+"_"+str(height)+".cell","w") # Final file name determined here
    for line in open("Temp", "r"):      # Iterates through each line in the Temp file
        if line not in lines:
            f.write(line)               # Writes line to both the set and the new file only if it hasn't been seen before
            lines.add(line)
    f.close()
    os.remove("Temp")                   # Deletes the Temp file so folders don't get cluttered
    
#GET USER INPUTS AND DEFINE VARIABLES
structure = get_string(["p","w","r","t"],"Which type of structure? ('p' for pillar, 'w' for wall, 'r' for nano-ridge, 't' for nano-pyramid)")

width = get_num("Input the width of the structure (will be rounded to a whole number)","int")

if structure == "p" or structure == "w":
    height = get_num("Input the height of the structure (will be rounded to a whole number)","int")
else: height = width/2                  # If a triangular structure is chosen, height will be defined by the width
    
if structure == "p" or structure == "t":
    depth = get_num("Input depth of structure (will be rounded to a whole number)","int")   # If pillar or pyramid is chosen, a second dimention is needed 'depth'
else: depth=1 

if structure == "t" and depth/2 < height: height = depth/2                                  # If nano-pyramid is chosen, height will be determined by the smaller of depth and width

mem_thick = get_num("Input thickness of membrane (will be rounded to a whole number)","int")

space1 = get_num("Input spacing between structures (will be rounded to a whole number)","int")

if structure == "p" or structure == "t":
    space2 = get_num("Input spacing between structuress in the 2nd dimention (will be rounded to a whole number)","int") #If pillar or pyramid is chosen, a second spacing dimention is needed
else: space2=0

vac = get_num("Input thickness of vaccum","float")                                          # vacuum thickness need not be a whole number (Although as floating point errors exist, one might want to force an integer)

passiv = get_string(["y","n"],"Would you like to passivate the structure? y/n?")

a, b, c = width+space1, depth+space2, vac+mem_thick+height                                  #These correspond to supercell dimentions/vectors (jmol helps visualise these further)
###

### BOOLEAN CONDITIONAL FUNCTIONS ###
def below_mem():
    if atom[2]+z <= 0: return True      #Returns true if the atom in question is below the membrane
    else: return False
    
def above_mem():
    if (((atom[2]+z) >=mem_thick-Hoff and (atom[2]+z) <=mem_thick) and ((atom[1]+y) >= depth-Hoff or (atom[0]+x) >= width-Hoff)): return True
    else: return False                  #Returns true if atom is above membrane and not in the area the structure is being generated
    
def in_supercell():
    if ((atom[1]+y) >= 0 and (atom[0]+x) >= 0): return True
    else: return False                  #Returns true if atom x and y co-ords are not negative (atom is in supercell bounds)

def not_structure_border_x():
    if (atom[1]+y) >= b-Hoff and (atom[0]+x) <= width: return False
    else: return True                   #Returns true if the atom won't collide with the structure at the border of supercell at the x boundry

def not_structure_border_y():
    if (atom[0]+x) >= a-Hoff and (atom[1]+y) <= depth: return False
    else: return True                   #Returns true if the atom won't collide with the structure at the border of supercell at the y boundry
###

# MEMBRANE SUBROUTINE
f = open("Temp","w+")                           # Filename is set to Temp for now, will always overwrite files.
f.write("%BLOCK LATTICE_CART \n"+str(lat_len*a)+"   0   0\n0   "+str(lat_len*b)+"   0\n0   0   "+str(lat_len*c)+"\n%ENDBLOCK LATTICE_CART\n\n%BLOCK POSITIONS_FRAC\n") # Writes supercell size/vectors to file
for z in range (0,mem_thick):                   #Iterates along z axis from 0 to thickness of membrane
    for y in range (0,b):                       #Iterates along y for each z value along the entire supercell (as the membrane extends over the entire supercell)
        for x in range (0,a):                   #Iterates along x for each y row along the entire supercell
            for atom in Sicell:                 #Iterates through each atom in the Silicon cell
                write_coords(atom,"Si")
if passiv == "y":
    if mem_thick == 1:                  #If membrane thickness is 1, the following for loop won't work, so it must be forced to iterate 1 more in the z direction
        z_max = mem_thick+1
    else:
        z_max = mem_thick
    for z in range (0,z_max,z_max-1):   #The third parameter here means increace in increments of membrane thickness (results in a top and bottom coating for the membrane only)
        for y in range (0,b):
            for x in range (0,a):
                for atom in Hcell:
                    if structure == "p" or structure == "w":
                        if below_mem() or ((above_mem() and in_supercell()) and not_structure_border_x() and not_structure_border_y()):
                            write_coords(atom,"H")
                    elif structure == "r":
                        if below_mem() or (((above_mem() and in_supercell()) and not_structure_border_x()) or (((atom[0]+x) < Hoff) and ((above_mem() and in_supercell())))):
                            write_coords(atom,"H")
                    else:
                        if below_mem() or (above_mem() and in_supercell()):
                            write_coords(atom,"H")
###

###BOOLEAN CONDITIONAL FUNCTIONS 2, ELECTRIC BOOGALOO###                            
def z_less_than_x(offset):
    if (atom[2]+z) <= (atom[0]+x) + (mem_thick)-offset: return True
    else: return False          #Returns true if the atom follows inequality z <= x + mem_thick - offset (left side of nano-ridge)

def z_less_than_neg_x(offset):
    if (atom[2]+z) <= -(atom[0]+x)+mem_thick + width-offset: return True
    else: return False          #Returns true if the atom follows inequality z <= -x + mem_thick + width - offset (right side of nano-ridge)

def z_less_than_y(offset):
    if (atom[2]+z) <= (atom[1]+y) + (mem_thick)-offset: return True
    else: return False          #Returns true if the atom follows inequality z <= y + mem_thick - offset (neg y side of nano-pyramid)

def z_less_than_neg_y(offset):
    if (atom[2]+z) <= -(atom[1]+y) + mem_thick + depth-offset: return True
    else: return False          #Returns true if the atom follows inequality z <= -y + mem_thick + depth - offset (pos y side of nano-pyramid)

def above_ridge_x():
    if ((atom[2]+z) >= (atom[0]+x) + (mem_thick)-2*Hoff) or ((atom[0]+x) + (atom[2]+z) >= mem_thick + width-2*Hoff): return True
    else: return False          #Returns true if the atom is above a nano-ridge in the x direction

def above_ridge_y():
    if ((atom[2]+z) >= (atom[1]+y) + (mem_thick)-2*Hoff) or ((atom[1]+y) + (atom[2]+z) >= mem_thick + depth-2*Hoff): return True
    else: return False          #Returns true if the atom is above a nano-ridge in the y direction (combine this with above for above nano-pyramid)

def H_ridge_boundry_x():
    if z_less_than_x(Hoff) and z_less_than_neg_x(Hoff): return True
    else: return False          #Returns true if the atom is below the hydrogen limit for a nano-ridge in x direction

def H_ridge_boundry_y():
    if z_less_than_y(Hoff) and z_less_than_neg_y(Hoff): return True
    else: return False          #Returns true if the atom is below the hydrogen limit for a nano-ridge in x direction (combine this with above for above nano-pyramid)

def H_above_pillar():
    if (atom[2]+z) >= mem_thick+height-Hoff and (atom[2]+z) <= mem_thick+height: return True
    else: return False          #Returns true if the atom is above the wall/pillar but below the hydrogen limit

def H_around_pillar_x():
    if ((atom[0]+x) >= width-Hoff and (atom[0]+x) <= width) or ((atom[0]+x) <= 0 and (atom[0]+x) >= -Hoff): return True
    else: return False          #Returns true if the atom is between the structure and hydrogen limit in the x direction

def H_around_pillar_y():
    if ((atom[1]+y) >= depth-Hoff and (atom[1]+y) <= depth) or ((atom[1]+y) >= -Hoff and (atom[1]+y) <= 0): return True
    else: return False          #Returns true if the atom is between the structure and hydrogen limit in the y direction
###
    
###STRUCTURE SUBROUTINE
if structure == "r":
    for z in range (mem_thick,mem_thick+width): 
        for y in range (0,depth):
            for x in range (0,width):
                for atom in Sicell:
                    if z_less_than_x(Sioff) and z_less_than_neg_x(Sioff):
                        write_coords(atom,"Si")
                if passiv == "y":
                    for atom in Hcell:
                        if above_ridge_x() and H_ridge_boundry_x() and ((atom[1]+y) >= 0):
                            write_coords(atom,"H")
elif structure == "t":
    for z in range (mem_thick,mem_thick+int(2*height)): 
        for y in range (0,depth):
            for x in range (0,width):
                for atom in Sicell:
                    if z_less_than_x(Sioff) and z_less_than_neg_x(Sioff) and z_less_than_y(Sioff) and z_less_than_neg_y(Sioff):
                        write_coords(atom,"Si")
                if passiv == "y":
                    for atom in Hcell:
                        if (above_ridge_x() or above_ridge_y()) and H_ridge_boundry_x() and H_ridge_boundry_y() and in_supercell():
                            write_coords(atom,"H")    
else:
    for z in range (mem_thick,mem_thick+height): 
        for y in range (0,depth):
            for x in range (0,width):
                for atom in Sicell:
                    write_coords(atom,"Si")
                if passiv == "y":
                    for atom in Hcell:
                        if (H_above_pillar() or H_around_pillar_x()) and ((atom[2]+z) > mem_thick):
                            write_coords(atom,"H")
                        if structure == "p":
                            if H_around_pillar_y() and (atom[2]+z) > mem_thick:
                                write_coords(atom,"H")
                                
f.write("%ENDBLOCK POSITIONS_FRAC")
f.close()
###

remove_duplicates()
