import os
lat_len = 5.43071 #Units : Angstroms
Sicell = [[0,0,0],[0,0.5,0.5],[0.25,0.25,0.25],[0.5,0,0.5],[0.25,0.75,0.75],[0.75,0.25,0.75],[0.75,0.75,0.25],[0.5,0.5,0]] # x,y,z fractional positions for the 8 Si atoms in the cell
Hcell = [[1,0,0.875],[0.125,0.875,1],[0.375,0.625,-0.25],[0.25,0.75,-0.125]]
Ocell = [[0.5,0.5,0.875],[0.75,0.25,-0.125],[0.875,0.125,0.875],[0.125,0.875,0.875],[0.375,0.625,-0.125],[0.125,0.825,-0.125]]

def write_coords(atom,species):
    f.write(str(species)+"   ")     #Defines Atom type
    for i in range(0,3):
        if i%3 == 0:
            f.write(str((1/a)*(atom[i]+x))+"   ") #Writes final fractional x co-ord in terms of supercell
        elif i%3 == 1:
            f.write(str((1/b)*(atom[i]+y))+"   ") #Writes y co-ord
        else:
            f.write(str((1/c)*(atom[i]+z))+"   ") #Writes z co-ord
    f.write("\n")                   #Line break

def get_string(allowed,question): #Asks question (a string) and checks if the answer is allowed with a list of allowed strings
    ans = None
    while ans not in allowed:
        ans =input(question).lower() # Forces input to be lower case so caps lock isn't an issue
    return ans

def get_num(question,form): #Asks question (a string) and converts it to a desired form ("int" for integer or anything else for float)
    while True:
        try: #This checks if the following lines of code work, if an error occurs, it moves to except
            value = float(input(question))
        except:
            print("Please ensure input is a number")
        break # This allows the code to break from the while loop if no error is found
    if form == "int":
        return int(round(value)) # If an integer is required, will round the float then convert it
    else:
        return value # Otherwise, the float is returned

def remove_duplicates():
    lines = set()
    f = open(structure+"_"+str(width)+"_"+str(depth)+"_"+str(height)+".cell","w")
    for line in open("Temp", "r"):
        if line not in lines:
            f.write(line)
            lines.add(line)
    f.close()
    os.remove("Temp")
    
#GET USER INPUTS AND DEFINE VARIABLES
structure = "Omem"
depth = width = height = 0
space1 = space2 = get_num("Input the width of the structure (will be rounded to a whole number)","int")
mem_thick = get_num("Input thickness of membrane (will be rounded to a whole number)","int")
vac = get_num("Input thickness of vaccum","float") # vacuum thickness need not be a whole number (Although as floating point errors exist, one might want to force an integer)
passiv = "y"

a, b, c = width+space1, depth+space2, vac+mem_thick+height           #These correspond to supercell dimentions (jmol helps visualise these further)
###

# MEMBRANE SUBROUTINE
f = open("Temp","w+") # Filename is set to Temp for now, will always overwrite files.
f.write("%BLOCK LATTICE_CART \n"+str(lat_len*a)+"   0   0\n0   "+str(lat_len*b)+"   0\n0   0   "+str(lat_len*c)+"\n%ENDBLOCK LATTICE_CART\n\n%BLOCK POSITIONS_FRAC\n") # Writes supercell size to file
for z in range (0,mem_thick):   #Iterates along z axis from 0 to thickness of membrane
    for y in range (0,b):       #Iterates along y for the entire supercell (as the membrane extends over the entire superccell)
        for x in range (0,a):   #Iterates along x for entire supercell
            for atom in Sicell: #Iterates through each atom in the cell
                write_coords(atom,"Si")
if passiv == "y":
    if mem_thick == 1:
        for y in range (0,b):
            for x in range (0,a):
                for atom in Hcell:
                    if ((atom[2]+z) <= 0) or ((atom[2]+z) >= mem_thick-0.2):
                        write_coords(atom,"H")
                for atom in Ocell:
                    if ((atom[2]+z) <= 0) or (((((atom[2]+z) >=mem_thick-0.2 and (atom[2]+z) <=mem_thick) and ((atom[1]+y) >= depth-0.2 or (atom[0]+x) >= width-0.2)) and ((atom[1]+y) >= 0 and (atom[0]+x) >= 0)) and not ((atom[1]+y) >= b-0.2 and (atom[0]+x) <= width) and not ((atom[0]+x) >= a-0.2 and (atom[1]+y) <= depth)):
                        write_coords(atom,"O")
    else:
        z_max = mem_thick
        for z in range (0,z_max,z_max-1): #The third parameter here means increace in increments of membrane thickness (results in a top and bottom coating for the membrane only)
            for y in range (0,b):
                for x in range (0,a):
                    for atom in Hcell:
                        if ((atom[2]+z) <= 0) or (((((atom[2]+z) >=mem_thick-0.2 and (atom[2]+z) <=mem_thick) and ((atom[1]+y) >= depth-0.2 or (atom[0]+x) >= width-0.2)) and ((atom[1]+y) >= 0 and (atom[0]+x) >= 0)) and not ((atom[1]+y) >= b-0.2 and (atom[0]+x) <= width) and not ((atom[0]+x) >= a-0.2 and (atom[1]+y) <= depth)):
                            write_coords(atom,"H")
                    for atom in Ocell:
                        if ((atom[2]+z) <= 0) or (((((atom[2]+z) >=mem_thick-0.2 and (atom[2]+z) <=mem_thick) and ((atom[1]+y) >= depth-0.2 or (atom[0]+x) >= width-0.2)) and ((atom[1]+y) >= 0 and (atom[0]+x) >= 0)) and not ((atom[1]+y) >= b-0.2 and (atom[0]+x) <= width) and not ((atom[0]+x) >= a-0.2 and (atom[1]+y) <= depth)):
                            write_coords(atom,"O")
f.write("%ENDBLOCK POSITIONS_FRAC")
f.close()
###

remove_duplicates()
