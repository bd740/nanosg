!Program will generate a Si membrane unit cell, constructed from Si conventional cells
!
!v. 0.1.2 Author: Ben Durham bd740@york.ac.uk   28/01/2020

!!Future development + feature ideas
! - option for Hydrogen passivation for pillars
! - option for triangular pillars as well as walls
! - triangle walls of all widths not just multiples of 2
! - passivate triangles
! - passivate by other elements as well as hydrogen e.g. oxygen.
! - option for Hexagonal pillars along different crystal direction


program make_pillar_cell
  use , intrinsic :: iso_fortran_env, only : int64, dp=>real64
  implicit none

  integer, parameter :: unit1=11  !file unit for output

  !Lattice parameters
  real(kind=dp), parameter :: Si_CC_lat_SW=5.43071000000_dp, Si_H_bond_len = 1.58_dp, Si_CC_lat_PBE=5.4681515_dp !Units : Angstroms
  real(kind=dp) :: Si_CC_lat
  !array containing the x, y & z fracional co-ordinates representing the postions of the Si atoms in a Si CC
  real(kind=dp), dimension(1:3,1:8) :: Si_CC_frac_pos
  real(kind=dp), dimension(:,:), allocatable :: Hydrogen_flat, Hydrogen_ridge
  real(kind=dp), dimension(1:3)  :: extra

  !rotational matrices
  real(kind=dp), dimension(:,:,:), allocatable :: rotation

  !integer number of CC in unit cell
  integer :: a, b, c, vacuum_thick, height, width, spacing, mem_thick, istat
  !integers for iteration
  integer ::  i ,j, k, atom
  character(len=1) :: structure, sub_structure

  !do we want hydrogen passivation or a sample cell
  logical :: passivating, example

  passivating = .false.
  example     = .false.
  Si_CC_lat   = Si_CC_lat_SW

  !check run time flags
  call check_args(passivating,example)

  ! define positions of atoms in conventional Si cell
  Si_CC_frac_pos(:,1)=(/0.00_dp,0.00_dp,0.00_dp/)
  Si_CC_frac_pos(:,2)=(/0.00_dp,0.50_dp,0.50_dp/)
  Si_CC_frac_pos(:,3)=(/0.25_dp,0.25_dp,0.25_dp/)
  Si_CC_frac_pos(:,4)=(/0.50_dp,0.00_dp,0.50_dp/)
  Si_CC_frac_pos(:,5)=(/0.25_dp,0.75_dp,0.75_dp/)   ! highest in z dierction
  Si_CC_frac_pos(:,6)=(/0.75_dp,0.25_dp,0.75_dp/)   !
  Si_CC_frac_pos(:,7)=(/0.75_dp,0.75_dp,0.25_dp/)
  Si_CC_frac_pos(:,8)=(/0.50_dp,0.50_dp,0.00_dp/)   ! in x-y plane


  if (passivating) call initialise_passivation()

  if (example) then
    height  = 2
    width   = 2
    spacing = 2
    mem_thick = 2
    vacuum_thick = 2
    structure = 'w'
    sub_structure = 'r'
  else
    call get_user_input()
  end if

    ! now input is known to be appropriate, prepare output file and lattice vectors
  if (structure=='w') then
    open (unit1, file="Si_wall_new.cell",iostat=istat)
    if (istat/=0) stop "Error, could not open Si_wall_new.cell"
    a = width + spacing                     ! a = width of wall + spacing of walls
    b = 1                                   ! b = 1 for walls
    c = height + mem_thick + vacuum_thick   ! c = height of wall + thickness of membrane + thickness of vacuum

  else if (structure=='p') then
    open (unit1, file="Si_pillar_new.cell",iostat=istat)
    if (istat/=0) stop "Error, could not open Si_pillar_new.cell"
    a = width + spacing                   ! a = width of pillar + spacing of pillars
    b = width + spacing                   ! b = width of pillar + spacing of pillars
    c = height + mem_thick + vacuum_thick ! c = height of pillar + thickness of membrane + thickness of vacuum

  end if

  ! print size of cell
  write(unit1,"(A)",iostat=istat) "%BLOCK LATTICE_CART"
  if (istat/=0) stop "Error, could not write to .cell file"
  write(unit1,"(3(F11.8,2x))",iostat=istat) Si_CC_lat*real(a,kind=dp), 0.0_dp, 0.0_dp
  if (istat/=0) stop "Error, could not write to .cell file"
  write(unit1,"(3(F11.8,2x))",iostat=istat) 0.0_dp, Si_CC_lat*real(b,kind=dp), 0.0_dp
  if (istat/=0) stop "Error, could not write to .cell file"
  write(unit1,"(3(F11.8,2x))",iostat=istat) 0.0_dp, 0.0_dp, Si_CC_lat*real(c,kind=dp)
  if (istat/=0) stop "Error, could not write to .cell file"
  write(unit1,"(A)",iostat=istat) "%ENDBLOCK LATTICE_CART"
  if (istat/=0) stop "Error, could not write to .cell file"

  write(unit1,*,iostat=istat)
  if (istat/=0) stop "Error, could not write to .cell file"

  !write position block header
  write(unit1,"(A)",iostat=istat) "%BLOCK POSITIONS_FRAC"
  if (istat/=0) stop "Error, could not write to .cell file"


  !!write atom positions

  ! z-dimension loop
  if (sub_structure=='r') then
    do k=0,c-1
      ! no atoms in the vacuum
      if (k.gt.vacuum_thick/2-1 .and. k.lt.(c-vacuum_thick/2))then
        ! y-dimension loop
        do j=0,b-1

          ! if making pillars  need to consider some other stuff
          if (structure=='p') then

            !if we are still forming the membrane or if we are within the indexs for the pillar
            !then we are allowed to write the CC

            if (k.lt.mem_thick + vacuum_thick/2 .or. (j.gt.spacing/2-1 .and. j.lt.spacing/2+width)) then
              ! x-dimension loop
              do i=0,a-1

                if (k.lt.mem_thick + vacuum_thick/2 .or. (i.gt.spacing/2-1 .and. i.lt.spacing/2+width)) then
                  call write_CC(i,j,k,a,b,c)
                end if

              end do
            end if

          else if (structure=='w') then  ! if making walls we good
            ! x-dimension loop
            do i=0,a-1

              if (k.lt.mem_thick + vacuum_thick/2 .or. (i.gt.spacing/2 - 1 .and. i.lt.spacing/2 + width)) then
                !need a  special case for corner of the pillar to avoid uinstable Si--H3
                if ((k==mem_thick + height + vacuum_thick/2 - 1) .and. (i == width +spacing/2 -1)) then
                  call write_CC(i,j,k,a,b,c,skip=6)
                else
                  call write_CC(i,j,k,a,b,c)
                end if
              end if

            end do  !x
          end if    !pillars or wall
        end do      !y
      end if        !no atoms in vacuum
    end do          !z


  !have to go about it differently for triangles
  else if (sub_structure=='t') then

    !z-dimension loop
    do k=0,c-1
      ! no atoms in the vacuum
      if (k .gt. vacuum_thick/2 - 1 .and. k.le.(c-vacuum_thick/2))then
        ! y-dimension loop
        do j=0,b-1

          if (structure=='w') then    ! if making walls then

            ! x-dimension loop
            do i=0,a-1


              if (k.lt. mem_thick + vacuum_thick/2) then
                call write_CC(i,j,k,a,b,c)

              else if (i .gt. spacing/2 - 1 .and. i .lt. spacing/2 + width) then
                call write_CC_triangle(i,j,k,a,b,c)
              end if
            end do   !x


          else if (structure=='p') then   !if we are making pillars, we can't
            ! to be implemented
          end if

        end do !y
      end if !no atoms in vacuum
    end do  !z

  end if

  if (passivating) then
    !doing the bottom of the membrane
    k = vacuum_thick/2

    ! y-dimension loop
    do j=0,b-1
      ! x-dimension loop
      do i=0,a-1
        call write_hydrogen(hydrogen_flat,i,j,k,a,b,c,3)
      end do  !x
    end do    !y
    !doing hydrogens after Si will be easier and better if we need to do stuff to phonon file
    !do we want to store all H pos's to check there will be no overlap?

    !doing nano-structured surface
    k = vacuum_thick/2 + mem_thick - 1

    ! y-dimension loop
    do j=0,b-1
      ! x-dimension loop
      do i=0,a-1

        if ( i.gt.spacing/2 - 1 .and. i.lt.spacing/2 + width) then
          if (i==spacing/2 + width - 1) then
            call write_hydrogen(hydrogen_ridge,i,j,k + height,a,b,c,3,skip=-1)
          else
            call write_hydrogen(hydrogen_ridge,i,j,k + height,a,b,c,3)
          end if
        else

          if (i==spacing/2-1) then
            call write_hydrogen(hydrogen_ridge,i,j,k,a,b,c,3,skip=1)
          else
            call write_hydrogen(hydrogen_ridge,i,j,k,a,b,c,3)
          end if

        end if
      end do  !x
    end do    !y

    !now do sides of the nanowall
    i = spacing/2

    do j = 0, b-1
      do k = (mem_thick + vacuum_thick/2),(mem_thick + vacuum_thick/2 + height - 1)

      !hydrogens where nanowall attaches to membrane
      if (k==mem_thick + vacuum_thick/2) then
        call write_hydrogen(hydrogen_flat,i,j,k,a,b,c,1,skip=4)

        call write_hydrogen(hydrogen_ridge,i + width - 1,j,k,a,b,c,1)

      !hydrogen on the corner at the tip of the nanowall
      else if (k==mem_thick + height + vacuum_thick/2 - 1) then

        call write_hydrogen(hydrogen_flat,i,j,k,a,b,c,1)

        call write_hydrogen(hydrogen_ridge,i + width - 1,j,k,a,b,c,1,skip=-2)

      else

        call write_hydrogen(hydrogen_flat,i,j,k,a,b,c,1)

        call write_hydrogen(hydrogen_ridge,i + width - 1,j,k,a,b,c,1)

      end if

      end do
    end do

    i = i + width - 1
    j = 0
    k = mem_thick + vacuum_thick/2 - 1

    extra = rotate_hydrogen(hydrogen_ridge(:,4),rotation(:,:,1))
    write(unit1,"(2x,A,2x,3(F11.8,2x))",iostat=istat)"H", (extra(1)+real(i,kind=dp))/real(a,kind=dp),&
                                                        & (extra(2)+real(j,kind=dp))/real(b,kind=dp),&
                                                        & (extra(3)+real(k,kind=dp))/real(c,kind=dp)
    if (istat/=0) stop "Error, could not write to .cell file"
  end if

  ! write the rest of the cell parameters
  call write_cell_param

  !deallocate what we need to
  if (passivating) then
    deallocate(rotation)
    deallocate(Hydrogen_ridge)
    deallocate(Hydrogen_flat)

  end if
  close(unit1)

  contains

  subroutine initialise_passivation
    !subroutine that will allocate and set up arrays required for Hydrogen passivation of surfaces
    !v. 0.0.1 Author: Ben Durham bd740@york.ac.uk  05/02/2020

    use , intrinsic :: iso_fortran_env, only : int64, dp=>real64
    implicit none

    integer :: istat
    real(kind=dp) :: length
    real(kind=dp), dimension(1:3) :: bond

    allocate(rotation(1:3,1:3,1:3),stat=istat)
    if(istat/=0) stop "pillar_cell.f90:: Failure allocating array for rotational matrices"
    allocate(Hydrogen_flat(1:3,1:4), stat=istat)
    if(istat/=0) stop "pillar_cell.f90:: Failure allocating array for Hydrogen postions"
    allocate(Hydrogen_ridge(1:3,1:4),stat=istat)
    if(istat/=0) stop "pillar_cell.f90:: Failure allocating array for Hydrogen postions"

    !reset lattice parameter
    Si_CC_lat = Si_CC_lat_PBE

    !define rotation matrices
    !rotates c direction to a direction
    rotation(:,1,1) = (/0.0_dp,1.0_dp,0.0_dp/)
    rotation(:,2,1) = (/0.0_dp,0.0_dp,1.0_dp/)
    rotation(:,3,1) = (/1.0_dp,0.0_dp,0.0_dp/)

    !rotates c direction to b direction
    rotation(:,1,2) = (/0.0_dp,0.0_dp,1.0_dp/)
    rotation(:,2,2) = (/1.0_dp,0.0_dp,0.0_dp/)
    rotation(:,3,2) = (/0.0_dp,1.0_dp,0.0_dp/)

    !c2c (identity)
    rotation(:,1,3) = (/1.0_dp,0.0_dp,0.0_dp/)
    rotation(:,2,3) = (/0.0_dp,1.0_dp,0.0_dp/)
    rotation(:,3,3) = (/0.0_dp,0.0_dp,1.0_dp/)

    !!determine positions of ths Si atoms the hydrogens are replacing
    !Hydrogen_flat will be applied to passivate the flat side of the Si CC
    Hydrogen_flat(:,1) = ( Si_CC_frac_pos(:,5) - (/0.0_dp,0.0_dp,1.0_dp/) )
    Hydrogen_flat(:,2) = ( Si_CC_frac_pos(:,6) - (/0.0_dp,0.0_dp,1.0_dp/) )
    Hydrogen_flat(:,3) = ( Si_CC_frac_pos(:,5) - (/0.5_dp,0.5_dp,1.0_dp/) )
    Hydrogen_flat(:,4) = ( Si_CC_frac_pos(:,6) - (/0.5_dp,0.5_dp,1.0_dp/) )

    !Hydrogen_ridge will be applied to passivate the ridged side of the Si CC
    Hydrogen_ridge(:,1) = (/1.0_dp,0.0_dp,1.0_dp/)
    Hydrogen_ridge(:,2) = (/0.5_dp,0.5_dp,1.0_dp/)
    Hydrogen_ridge(:,3) = (/0.5_dp,0.5_dp,1.0_dp/)
    Hydrogen_ridge(:,4) = (/0.0_dp,1.0_dp,1.0_dp/)

    !determine length of the bond in Angstroms
    bond   = (Si_CC_frac_pos(:,8) - Hydrogen_flat(:,1))
    length = sqrt( bond(1)**2 + bond(2)**2 + bond(3)**2 )

    !shorten the bonds so they of appropriate length
    Hydrogen_flat(:,1) = Si_CC_frac_pos(:,8) - ( bond(:) * Si_H_bond_len ) / ( length * Si_CC_lat )
    bond               = (Si_CC_frac_pos(:,8) - Hydrogen_flat(:,2))
    Hydrogen_flat(:,2) = Si_CC_frac_pos(:,8) - ( bond(:) * Si_H_bond_len ) / ( length * Si_CC_lat )
    bond               = (Si_CC_frac_pos(:,1) - Hydrogen_flat(:,3))
    Hydrogen_flat(:,3) = Si_CC_frac_pos(:,1) - ( bond(:) * Si_H_bond_len ) / ( length * Si_CC_lat )
    bond               = (Si_CC_frac_pos(:,1) - Hydrogen_flat(:,4))
    Hydrogen_flat(:,4) = Si_CC_frac_pos(:,1) - ( bond(:) * Si_H_bond_len ) / ( length * Si_CC_lat )

    bond                = (Si_CC_frac_pos(:,6) - Hydrogen_ridge(:,1))
    Hydrogen_ridge(:,1) = Si_CC_frac_pos(:,6) - ( bond(:) * Si_H_bond_len ) / ( length * Si_CC_lat )
    bond                = (Si_CC_frac_pos(:,6) - Hydrogen_ridge(:,2))
    Hydrogen_ridge(:,2) = Si_CC_frac_pos(:,6) - ( bond(:) * Si_H_bond_len ) / ( length * Si_CC_lat )
    bond                = (Si_CC_frac_pos(:,5) - Hydrogen_ridge(:,3))
    Hydrogen_ridge(:,3) = Si_CC_frac_pos(:,5) - ( bond(:) * Si_H_bond_len ) / ( length * Si_CC_lat )
    bond                = (Si_CC_frac_pos(:,5) - Hydrogen_ridge(:,4))
    Hydrogen_ridge(:,4) = Si_CC_frac_pos(:,5) - ( bond(:) * Si_H_bond_len ) / ( length * Si_CC_lat )

    return
  end subroutine

  subroutine get_user_input
    !subroutine that asks user for input and check that input is appropriate
    !v. 0.0.1 Author: Ben Durham bd740@york.ac.uk  05/02/2020

    use , intrinsic :: iso_fortran_env, only : int64, dp=>real64
    implicit none

    character(len=20) :: structure_name

    !get structure
    print *, "Input structure 'p' or 'w' (pillar or wall)"
    read  *, structure
    if (.not. (structure=='p' .or. structure=='w')) stop "Input is GARBAGE, please try again"

    !get sub structure
    print *, "Input sub_structure 'r' or 't' (rectangular or triangular)"
    read  *, sub_structure
    if (.not. (sub_structure=='r' .or. sub_structure=='t')) stop "Input is GARBAGE, please try again"
    if (structure=='p' .and. sub_structure=='t') stop "Triangular pillars has not yet been implemented, sorry"

    if (passivating .and. (structure=='p' .or. sub_structure=='t')) stop "Hydrogen passivation not supported for &
                                                                      &this structure yet, turn off -p flag"

    if (structure=='w') then
      structure_name = "walls"
    else if (structure=='p') then
      structure_name = "pillars"
    end if


    if (sub_structure=='r') then
      print *, "Input height of the ", trim(structure_name), " in CC units"
      read  *, height
    end if

    print *, "Input width of the ", trim(structure_name), " in CC units"
    if (sub_structure=='r') then
      print *, "(please not width of 1, not geometrically stable but SW will think it is)"
      read  *, width
      if (width==1) print *, "Width of 1 chosen, are you sure about that?"
    else if (sub_structure=='t') then
      read  *, width
      !set height of nano-structure
      height = width / 2
    end if

    print *, "Input spacing between the ", trim(structure_name), " in CC units"
    print *, "(mulitple of two please, for now)... (zero is also allowed)"
    read  *, spacing
    if (mod(spacing,2)/=0) stop "I said a multiple of two, program has exited, try again"

    print *, "Input thickness of membrane in CC units (thickness along z-direction) [>0]"
    read  *, mem_thick

    print *, "Input thickness of vacuum in CC units (along z-direction)"
    print *, "(mulitple of two please, for now)... (zero is also allowed)"
    read  *, vacuum_thick
    if (mod(vacuum_thick,2)/=0) stop "I said a multiple of two, program has exited, try again"

    ! check input is appropriate
    if (height<0 .or. width<0 .or. spacing<0 .or. mem_thick<=0 .or. vacuum_thick<0) then
      print *, "Error: Negative input, program has exitted try again please"
      stop
    end if

    return
  end subroutine

  function rotate_hydrogen(hydrogen_pos,rotation_mat)
    !function that will apply a rotation matrix to a 3D Hydrogen position.
    !arguments  : array containing postion of the hydrogen, rotation matrix to be applied.
    !returns    : array containing the rotated hydrogen postition.
    !v. 0.0.1 Author: Ben Durham bd740@york.ac.uk  05/02/2020

    use , intrinsic :: iso_fortran_env, only : int64, dp=>real64
    implicit none
    !arguements
    real(kind=dp), dimension(1:3)     :: hydrogen_pos
    real(kind=dp), dimension(1:3,1:3) :: rotation_mat
    !returns
    real(kind=dp), dimension(1:3)     :: rotate_hydrogen

    rotate_hydrogen(:) = matmul(rotation_mat,hydrogen_pos)

    return
  end function

  function triangle_plane(x)
    !function that will determine the line equation in x-z plane in fractional co-ords for which forms
    !arguments: width in fractional co-ords
    !return:: triangle_plane: z value for a position in x, can then be used to determine iof we are in the triangle or not
    !                          i.e. if we are at a point along x, is my z co-ord above or below the line
    !v. 0.0.2 Author: Ben Durham bd740@york.ac.uk  30/01/2020

    ! AU-toblerone wall diagram
    !
    !             <--(centre,tri_tip)
    !           /\
    !          /  \
    !         /    \
    !        /      \
    !_______/        \______  y=mem_top
    !       |        |
    !     start     end

    use , intrinsic :: iso_fortran_env, only : int64, dp=>real64
    implicit none
    real(kind=dp) :: x, start, finish, centre, gradient, tri_tip, mem_top  !fractional coordinate of atom in x-dimension
    !return value
    real(kind=dp) :: triangle_plane


    ! not quite what we want yet but close

    start   = real(spacing/2,kind=dp) / real(a,kind=dp)
    centre  = 0.5_dp
    finish  = start + (real(width,kind=dp)) / real(a,kind=dp)

    mem_top = real(mem_thick + vacuum_thick / 2,kind=dp) / real(c,kind=dp)
    if (mod(width,2) == 0) then
      tri_tip = mem_top + ( real(height,kind=dp) - 0.1_dp) / real(c,kind=dp)
    else
      tri_tip = mem_top + ( real(height,kind=dp) + 0.4_dp) / real(c,kind=dp)
    end if
    !need the gradient
    gradient = (tri_tip - mem_top) / (centre - start)

    if ( (x .ge. start) .and. (x .le. centre) ) then
      triangle_plane = mem_top + (x-start) * gradient
    else if ( (x .ge. centre) .and. (x .le. finish) ) then
      gradient = -gradient
      triangle_plane = tri_tip + (x-centre) * gradient
    else
      triangle_plane = mem_top
    end if

    return
  end function

  subroutine write_CC_triangle(i,j,k,a,b,c)
    !subroutine which writes all the additional parameters we need to run a phonon calculation
    !arguements: i,j,k, define the where within the unit cell the CC we are writing is
    !arguements: a,b,c, define the size of the unit cell
    !v. 0.0.1 Author: Ben Durham bd740@york.ac.uk  28/01/2020

    use , intrinsic :: iso_fortran_env, only : int64, dp=>real64
    implicit none
    !arguements
    integer :: i, j, k    !define the where in space the CC we are writing is
    integer :: a, b, c    !defines the size of the unit cell and thus fractional postions

    !internals
    integer :: atom
    real(kind=dp) :: x, z

    ! atom in CC loop
    do atom=1,8

      !work out fractional co-ord in x and z direction
      x = (Si_CC_frac_pos(1,atom)+real(i,kind=dp)) / real(a,kind=dp)
      z = (Si_CC_frac_pos(3,atom)+real(k,kind=dp)) / real(c,kind=dp)

      !only write atom location if z is below the plane defined
      if ( z .lt. triangle_plane(x) ) then
        write(unit1,"(2x,A,3(F11.8,2x))",iostat=istat)"Si", x,(Si_CC_frac_pos(2,atom)+real(j,kind=dp)) / real(b,kind=dp), z
        if (istat/=0) stop "Error, could not write to .cell file"

      end if
    end do

    return
  end subroutine

  subroutine write_CC(i,j,k,a,b,c,skip)
    !subroutine which writes a Si CC at location i, j, k in the unit cell
    !alters the fractional positions so the c
    !arguements : i,j,k, define the where within the unit cell the CC we are writing is
    !           : a,b,c, define the size of the unit cell
    !           : skip (optional), index of the atom to skip over, if we are passivating the surface
    !             skipped atom is replaced by hydrogen
    !v. 0.0.2 Author: Ben Durham bd740@york.ac.uk  12/02/2020

    implicit none
    !arguements
    integer :: i, j, k    !define the where in space the CC we are writing is
    integer :: a, b, c    !defines the size of the unit cell and thus fractional postions
    integer, optional :: skip
    !internals
    integer       :: atom, skipping

    if (present(skip)) then
      skipping = skip
    else
      skipping=0
    end if

    ! atom in CC loop
    do atom=1,8
      if (atom/=skipping) then
        write(unit1,"(2x,A,2x,3(F11.8,2x))",iostat=istat)"Si", (Si_CC_frac_pos(1,atom)+real(i,kind=dp))/real(a,kind=dp),&
                                                          & (Si_CC_frac_pos(2,atom)+real(j,kind=dp))/real(b,kind=dp),&
                                                          & (Si_CC_frac_pos(3,atom)+real(k,kind=dp))/real(c,kind=dp)
        if (istat/=0) stop "Error, could not write to .cell file"
      else if (atom==skipping .and. passivating) then
        write(unit1,"(2x,A,2x,3(F11.8,2x))",iostat=istat)"H", (Si_CC_frac_pos(1,atom)+real(i,kind=dp))/real(a,kind=dp),&
                                                          & (Si_CC_frac_pos(2,atom)+real(j,kind=dp))/real(b,kind=dp),&
                                                          & (Si_CC_frac_pos(3,atom)+real(k,kind=dp))/real(c,kind=dp)
        if (istat/=0) stop "Error, could not write to .cell file"
      end if
    end do

    return
  end subroutine

  subroutine write_hydrogen(hydrogens,i,j,k,a,b,c,rotation_idx,skip)
    !subroutine
    !arguements : hydrogens, contains the postions of the hydrogens we are printing
    !           : i,j,k, define the where within the unit cell the CC we are writing is
    !           : a,b,c, define the size of the unit cell
    !           : rotation, (int 1->3) defines the direction of rotation
    !           : skip (optional), index of the atom to skip over, can also be -1 or -2 which skips two
    !             hydrogens bonded to the Si atom
    !internals  : rotated, contains atoms postions after rotation
    !           : skipping, index of atom being skipped, default is 0
    !v. 0.0.3 Author: Ben Durham bd740@york.ac.uk  12/02/2020

    implicit none
    !arguements
    real(kind=dp), dimension(:,:) :: hydrogens
    integer       :: i, j, k    !define the where in space the CC we are writing is
    integer       :: a, b, c    !defines the size of the unit cell and thus fractional postions
    integer       :: rotation_idx
    integer, optional :: skip
    !internals
    integer       :: atom, skipping_first, skipping_second
    real(kind=dp), dimension(1:3) :: rotated

    if (present(skip)) then
      if (skip>0) then
        skipping_first = skip
        skipping_second = 0
      else if (skip<0) then
        skipping_first = abs(2*skip) - 1
        skipping_second = abs(2*skip)
      end if
    else
      skipping_first = 0
      skipping_second = 0
    end if

    ! atom in CC loop
    do atom=1,4
      if (atom/=skipping_first .and. atom/=skipping_second) then
        rotated(:) = rotate_hydrogen(hydrogens(:,atom),rotation(:,:,rotation_idx))

        write(unit1,"(2x,A,2x,3(F11.8,2x))",iostat=istat)"H", (rotated(1)+real(i,kind=dp))/real(a,kind=dp),&
                                                            & (rotated(2)+real(j,kind=dp))/real(b,kind=dp),&
                                                            & (rotated(3)+real(k,kind=dp))/real(c,kind=dp)
        if (istat/=0) stop "Error, could not write to .cell file"
      end if
    end do

    return
  end subroutine

  subroutine write_cell_param
    !subroutine which writes all the additional parameters we need to run a phonon calculation
    !no arguements
    !v. 0.0.1 Author: Ben Durham bd740@york.ac.uk  28/01/2020
    !
    !!Future development + features
    ! - may wish to include feature to that calculates k-point spacing to give good sampling (~40-50 points)
    ! - may wish to use psuedopot file names in future for DFT calcs
    ! - may need other cell parameters for DFT calcs

    implicit none
    integer :: istat
    !unit1 is a parameter and therefore does not need to be passed to subroutine

    !end position block first
    write(unit1,"(A)",iostat=istat) "%ENDBLOCK POSITIONS_FRAC"
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat)
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "%BLOCK SPECIES_POT"                  ! psuedopot block
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "  NCP"
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "%ENDBLOCK SPECIES_POT"
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat)
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "kpoint_mp_grid : 1 1 1"              ! kpiont grid
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat)
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "phonon_kpoint_mp_grid : 2 4 1"       ! qpoint grid
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "phonon_kpoint_mp_offset : 0.250 0.125 0.000"
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat)
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "phonon_fine_kpoint_path_spacing : 0.01" ! fine qpoint spacing
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat)
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "%Block phonon_fine_kpoint_path"      ! fine qpoint path
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "  0.0 0.0 0.0"
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "  0.5 0.0 0.0"
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "  0.5 0.5 0.0"
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "  0.0 0.0 0.0"
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "%endBlock phonon_fine_kpoint_path"
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat)
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "%block cell_devel_code" !tell code not to bother looking for symmetry
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "P1_SYMMETRY"
    if (istat/=0) stop "Error, could not write to .cell file"
    write(unit1,"(A)",iostat=istat) "%endblock cell_devel_code"
    if (istat/=0) stop "Error, could not write to .cell file"


    return
  end subroutine

  subroutine check_args(passivating, example)
    implicit none
    character(len=32)           :: arg
    integer                     :: i
    logical                     :: passivating, example

    do i = 1, command_argument_count()
      call get_command_argument(i, arg)

      select case (arg)
        case ('-v', '--version')
          print '(a)', 'version 0.1.2, Author: Ben Durham, email: bd740@york.ac.uk'
          call exit(0)
        case ('-h', '--help')
          call print_help()
          call exit(0)
        case ('-p', '--passivate')
          print '(a)', "Hydrogen passivation switched on"
          passivating = .true.
        case ('-e','--example')
          print '(a)', "Generating example unit cell as a test"
          example = .true.
        case default
          print '(2a, /)', 'Unrecognised command-line option: ', arg
          call print_help()
          call exit(0)
      end select

    end do

    return
  end subroutine

  subroutine print_help()
    implicit none

    !tell the user what the code will do
    print '(a)', "This program will generate a unit cell of a nanostuctured Si membrane constructed from Si conventional"
    print '(a)', "cells for a CASTEP phonon calculation, it should include all the relavant cell parameters however phonon"
    print '(a)', "fine kpoint path spacing may need adjusting. This program will write the new unit cell file to a file"
    print '(a)', "called Si_wall_new.cell or Si_pillar_new.cell depending on the structure chosen. If a file already exists"
    print '(a)', "with this name, it will be overwritten."
    print '(a)',
    !tell user about running options
    print '(a, /)', 'command-line options:'
    print '(a)',    '  -v, --version      print version information and exit'
    print '(a)',    '  -h, --help         print this message'
    print '(a)',    '  -p, --passivate    return .cell file for which surfaces have been hydrogen passivated'
    print '(a)',    '  -e, --example      return an example unit .cell file of a nanowall'

    return
  end subroutine print_help

end program
