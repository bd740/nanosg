lat_len = 5.43071 #Units : Angstroms
Sicell = [[0,0,0],[0,0.5,0.5],[0.25,0.25,0.25],[0.5,0,0.5],[0.25,0.75,0.75],[0.75,0.25,0.75],[0.75,0.75,0.25],[0.5,0.5,0]] # x,y,z fractional positions for the 8 atoms in the cell
Hcell = [[1,0,0.875],[0.125,0.875,1],[0.375,0.625,-0.25],[0.25,0.75,-0.125]]
Ocell = [[0.5,0.5,0.875],[0.75,0.25,-0.125],[0.875,0.125,0.875],[0.125,0.875,0.875],[0.375,0.625,-0.125],[0.125,0.825,-0.125]]
#Hcell = [list(i) for i in (set(tuple(coords) for coords in Hcell))]        #Easy code to remove duplicates

def test_cell():
    f = open("OxygenTestCell2.cell","w+")
    f.write("%BLOCK LATTICE_CART \n"+str(lat_len)+"   0   0\n0   "+str(lat_len)+"   0\n0   0   "+str(lat_len)+"\n%ENDBLOCK LATTICE_CART\n\n%BLOCK POSITIONS_FRAC\n")
    for i in Sicell:                               ###Code generates a single cell (FOR DEBUGGING)
        f.write("Si   ")
        for g in i:
            f.write(str(g)+"   ")
        f.write("\n")
    for i in Ocell:                               ###Code generates a single O cell (FOR DEBUGGING)
        f.write("O   ")
        for g in i:
            f.write(str(g)+"   ")
        f.write("\n")
    for i in Hcell:
        f.write("H   ")
        for g in i:
            f.write(str(g)+"   ")
        f.write("\n")
    f.write("%ENDBLOCK POSITIONS_FRAC")
    f.close()


test_cell()
